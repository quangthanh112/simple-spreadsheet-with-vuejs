const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const gulpHtmlBeautify = require('gulp-html-beautify');
const browserSync = require('browser-sync');
const sassLint = require('gulp-sass-lint');
const del = require('del');
const $ = gulpLoadPlugins();
const htmlhint = require("gulp-htmlhint");
const reload = browserSync.reload;

gulp.task('styles', () => {
  return gulp.src('app/assets/scss/*.scss')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 20 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/assets/css'))
    .pipe(reload({stream: true}));
});

gulp.task('sassLint', function() {
  return gulp.src(['app/assets/scss/**/*.scss', '!app/assets/scss/component/core/_normalize.scss', '!app/assets/scss/component/core/_scaffolding.scss', '!app/assets/scss/style.scss', '!app/assets/scss/component/_icofont.scss', '!app/assets/scss/component/mixins/**/*.scss'])
    .pipe(sassLint())
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
});

gulp.task('scripts', () => {
  return gulp.src('app/assets/js/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/assets/js'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/assets/js/**/*.js', {
    fix: true
  })
    .pipe(gulp.dest('app/assets/js'));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js', {
    fix: true,
    env: {
      mocha: true
    }
  })
    .pipe(gulp.dest('test/spec/**/*.js'));
});

gulp.task('html', ['views', 'styles', 'scripts'], () => {
  return gulp.src(['app/**/*.html', '.tmp/**/*.html'])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if('*.html', gulpHtmlBeautify({indentSize: 2})))
    .pipe(gulp.dest('dist'));
});

gulp.task('views', () => {
  return gulp.src(['app/**/*.jade', '!app/templates/**/*.jade'])
    .pipe($.plumber())
    .pipe($.jade({pretty: true}))
    .pipe(gulp.dest('.tmp'))
		.pipe(htmlhint())
    .pipe(htmlhint.reporter())
    .pipe(reload({stream: true}));
});

gulp.task('images', () => {
  return gulp.src('app/assets/img/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{cleanupIDs: false}]
    })))
    .pipe(gulp.dest('dist/assets/img'));
});

gulp.task('fonts', () => {
  return gulp.src('app/assets/fonts/**/*')
    .pipe(gulp.dest('.tmp/assets/fonts'))
    .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('vendor', () => {
  return gulp.src('app/assets/js/vendor/html5-3.6-respond-1.4.2.min.js')
    .pipe(gulp.dest('dist/assets/js/vendor'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*.*',
	'!app/.idea/*.*',
    '!app/**/*.html',
    '!app/**/*.jade'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['views', 'styles', 'sassLint', 'scripts', 'fonts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch([
    'app/**/*.html',
    'app/assets/img/**/*',
    '.tmp/assets/fonts/**/*'
  ]).on('change', reload);

  gulp.watch('app/**/*.jade', ['views']);
  gulp.watch('app/assets/scss/**/*.scss', ['styles', 'sassLint']);
  gulp.watch('app/assets/js/**/*.js', ['scripts']);
  gulp.watch('app/assets/fonts/**/*', ['fonts']);
});

gulp.task('serve:dist', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:vuejs', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['vuejs']
    }
  });
  gulp.watch([
    'vuejs/**/*.html',
    'vuejs/assets/img/**/*',
    '.tmp/assets/fonts/**/*'
  ]).on('change', reload);

  gulp.watch('vuejs/**/*.jade', ['views']);
  gulp.watch('vuejs/assets/scss/**/*.scss', ['styles', 'sassLint']);
  gulp.watch('vuejs/assets/js/**/*.js', ['scripts']);
  gulp.watch('vuejs/assets/fonts/**/*', ['fonts']);
});

gulp.task('serve:test', ['scripts'], () => {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/assets/js',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/assets/js/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
});


gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras', 'vendor'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
