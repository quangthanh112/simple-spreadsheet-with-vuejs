// **********************************************************************//
// ! Filter Global
// **********************************************************************//
Vue.filter('format', function (value, toFix = 0) {
	if (!value) return ''
	value = +value;
	return (value % 1 !== 0) ? value.toFixed(toFix) : value;
});

// **********************************************************************//
// ! Component Ingredient Header => Displays the label of the column
// **********************************************************************//
Vue.component('ingredient-head', {
	props: {
		title: {
			type: Object,
			required: true,
			validator: function (obj) {
				return (
					obj.status && obj.status.length &&
					obj.type && obj.type.length &&
					obj.materialNo && obj.materialNo.length &&
					obj.ingredientName && obj.ingredientName.length &&
					obj.percentage && obj.percentage.length &&
					obj.weight && obj.weight.length &&
					obj.status && obj.status.length &&
					obj.volume && obj.volume.length &&
					obj.starch && obj.starch.length &&
					obj.totalFat && obj.totalFat.length &&
					obj.toalEnergy && obj.toalEnergy.length &&
					obj.sugar && obj.sugar.length &&
					obj.sodium && obj.sodium.length &&
					obj.acidityLevel && obj.acidityLevel.length
				);
			}
		}
	},
	template: `
		<tr>
			<th>{{title.status}}</th>
			<th>{{title.type}}</th>
			<th>{{title.materialNo}}</th>
			<th>{{title.ingredientName}}</th>
			<th>{{title.percentage}}</th>
			<th>{{title.weight}}</th>
			<th>{{title.volume}}</th>
			<th>{{title.density}}</th>
			<th>{{title.starch}}</th>
			<th>{{title.totalFat}}</th>
			<th>{{title.toalEnergy}}</th>
			<th>{{title.sugar}}</th>
			<th>{{title.sodium}}</th>
			<th>{{title.acidityLevel}}</th>
		</tr>
	`
});


// **********************************************************************//
// ! Component Ingredient foot => Displays the total of each column of the table
// **********************************************************************//
Vue.component('ingredient-foot', {
	props: {
		weight: {
			type: Number,
			required: true,
			default: 0
		},
		volume: {
			type: Number,
			required: true,
			default: 0
		},
		starch: {
			type: Number,
			required: true,
			default: 0
		},
		fat: {
			type: Number,
			required: true,
			default: 0
		},
		energy: {
			type: Number,
			required: true,
			default: 0
		},
		sugar: {
			type: Number,
			required: true,
			default: 0
		},
		sodium: {
			type: Number,
			required: true,
			default: 0
		},
		acidity: {
			type: Number,
			required: true,
			default: 0
		}
	},
	template: `
		<tr>
			<td colspan="4">Total</td>
			<td>100</td>
			<td>{{weight | format(3)}}</td>
			<td>{{volume | format(3)}}</td>
			<td></td>
			<td>{{starch | format(3)}}</td>
			<td>{{fat | format(3)}}</td>
			<td>{{energy | format(3)}}</td>
			<td>{{sugar | format(3)}}</td>
			<td>{{sodium | format(3)}}</td>
			<td>{{acidity | format(3)}}</td>
		</tr>
	`
});
// **********************************************************************//
// ! Component Ingredient Item
// **********************************************************************//
Vue.component('ingredient-item', {
	filters: {
		capitalize: function (value) {
			if (!value) return '';
			value = value.toString();
			return value.toUpperCase();
		},
		empty: function (value) {
			return value === 0 ? '' : value;
		}
	},
	props: {
		id: {
			type: Number,
			required: true
		},
		status: {
			type: String,
			required: true,
			validator: function (value) {
				return [
					'lock',
					'unlock',
					'warning',
					'deactive'
				].indexOf(value) !== -1;
			}
		},
		type: {
			type: String,
			required: true,
			validator: function (value) {
				return [
					'B',
					'F',
					''
				].indexOf(value.toUpperCase()) !== -1;
			}
		},
		materialNo: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		ingredientName: {
			type: String,
			required: true
		},
		weight: {
			type: Number,
			required: true,
			default: 0,
			validator: function (value) {
				return value >= 0
			}
		},
		density: {
			type: Number,
			required: true,
			default: 0,
			validator: function (value) {
				return value >= 0
			}
		},
		starch: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		totalFat: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		toalEnergy: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		sugar: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		sodium: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		acidityLevel: {
			type: Number,
			required: true,
			validator: function (value) {
				return value >= 0
			}
		},
		total: {
			type: Number,
			required: true,
			default: 0
		},
		index: {
			type: Number,
			default: 0
		}
	},
	data: function () {
		return {
			item: {
				id: this.id,
				status: this.status,
				type: this.type,
				materialNo: this.materialNo,
				ingredientName: this.ingredientName,
				weight: this.weight,
				density: this.density,
				starch: this.starch,
				totalFat: this.totalFat,
				toalEnergy: this.toalEnergy,
				sugar: this.sugar,
				sodium: this.sodium,
				acidityLevel: this.acidityLevel
			},
			volume: this.weight / this.density,
			default: {
				weight: this.weight,
				volume: this.weight / this.density
			}
		}
	},
	computed: {
		editable: function () {
			return {
				weight: this.volume * this.item.density,
				volume: this.weight / this.item.density
			}
		},
		percentage: function () {
			return (this.weight / this.total * 100)
		}
	},
	methods: {
		changeInput: function (type, event) {
			var value = event.target;
			var newItem = Object.assign({}, this.item);

			if (isNaN(value.innerText)) {
				newItem['weight'] = this.default['weight'];
				value.innerText = this.default[type];
			} else {
				this.default[type] = value.innerText;
			}

			if (type === 'volume') {
				this.volume = parseFloat(value.innerText);
				this.item.weight = this.volume * this.item.density;
				newItem['weight'] = this.volume * this.item.density;
			} else {
				this.item.weight = parseFloat(value.innerText);
				newItem['weight'] = parseFloat(value.innerText);
			}
			this.$emit('update', newItem);
		}
	},
	template: `
		<tr>
			<td>{{ item.status }}</td>
			<td>{{ item.type | capitalize }}</td>
			<td class="text-right">{{ item.materialNo | empty}}</td>
			<td>{{ item.ingredientName }}</td>
			<td class="text-right">{{ percentage | format(2) }}</td>
			<td class="text-right contenteditable" contenteditable="true" v-on:input="changeInput('weight', $event)">{{ editable.weight }}</td>
			<td class="text-right contenteditable" contenteditable="true" v-on:input="changeInput('volume', $event)">{{ editable.volume | format(3) }}</td>
			<td class="text-right">{{ density }}</td>
			<td class="text-right">{{ item.starch * item.weight}}</td>
			<td class="text-right">{{ item.totalFat * item.weight}}</td>
			<td class="text-right">{{ item.toalEnergy * item.weight}}</td>
			<td class="text-right">{{ item.sugar * item.weight}}</td>
			<td class="text-right">{{ item.sodium * item.weight}}</td>
			<td class="text-right">{{ item.acidityLevel * item.weight}}</td>
		</tr>
	`
});

// **********************************************************************//
// ! Root Instance
// **********************************************************************//
var vm = new Vue({
	el: '#app',
	data: function () {
		return {
			labels: {
				status: 'Status',
				type: 'Type',
				materialNo: 'Material No.',
				ingredientName: 'Ingredient Name',
				percentage: '%',
				weight: 'Weight(g)',
				volume: 'Volume (ml)',
				density: 'Density',
				starch: 'Starch',
				totalFat: 'Total Fat',
				toalEnergy: 'Total Energy',
				sugar: 'Sugar',
				sodium: 'Sodium',
				acidityLevel: 'Acidity Level'
			},
			ingredients: [
				{
					id: 1,
					orderNo: 2,
					status: 'unlock',
					type: '',
					materialNo: 0,
					ingredientName: 'ART-2086 TEA EXTRACT LIQUID TURKEY',
					weight: 20,
					density: 4,
					starch: 7,
					totalFat: 8,
					toalEnergy: 9,
					sugar: 10,
					sodium: 11,
					acidityLevel: 12
				},
				{
					id: 2,
					orderNo: 5,
					status: 'lock',
					type: '',
					materialNo: 0,
					ingredientName: 'ART-2086 TEA EXTRACT LIQUID TURKEY',
					weight: 20,
					density: 2.5,
					starch: 5,
					totalFat: 6,
					toalEnergy: 7,
					sugar: 8,
					sodium: 9,
					acidityLevel: 10
				},
				{
					id: 3,
					orderNo: 6,
					status: 'deactive',
					type: '',
					materialNo: 99000002,
					ingredientName: 'GRAPE FLAVOUR',
					weight: 20,
					density: 1.5,
					starch: 4,
					totalFat: 6,
					toalEnergy: 4,
					sugar: 3,
					sodium: 2,
					acidityLevel: 1
				},
				{
					id: 4,
					orderNo: 1,
					status: 'lock',
					type: '',
					materialNo: 0,
					ingredientName: 'ART-2086 TAB WATER VIENNA',
					weight: 20,
					density: 2,
					starch: 1,
					totalFat: 2,
					toalEnergy: 3,
					sugar: 4,
					sodium: 5,
					acidityLevel: 6
				},
				{
					id: 5,
					orderNo: 3,
					status: 'warning',
					type: '',
					materialNo: 77771234,
					ingredientName: 'ART-2086 ASCORBIC ACID (E300)',
					weight: 20,
					density: 5,
					starch: 8,
					totalFat: 7,
					toalEnergy: 6,
					sugar: 5,
					sodium: 4,
					acidityLevel: 3
				},
				{
					id: 6,
					orderNo: 4,
					status: 'lock',
					type: 'B',
					materialNo: 77777777,
					ingredientName: 'PINEAPPLE (BRIX)',
					weight: 20,
					density: 0.5,
					starch: 2,
					totalFat: 1,
					toalEnergy: 1,
					sugar: 2,
					sodium: 3,
					acidityLevel: 4
				},
				{
					id: 7,
					orderNo: 7,
					status: 'lock',
					type: 'F',
					materialNo: 0,
					ingredientName: 'WATER (FILLER)',
					weight: 80,
					density: 1,
					starch: 4,
					totalFat: 5,
					toalEnergy: 6,
					sugar: 7,
					sodium: 8,
					acidityLevel: 9
				}
			]
		}
	},
	computed: {
		dataSort: function () {
			function compare(a, b) {
				if (a.orderNo < b.orderNo)
					return -1;
				if (a.orderNo > b.orderNo)
					return 1;
				return 0;
			}
			return this.ingredients.sort(compare);
		},
		summary: function () {
			var objDefault = {
				weight: 0,
				volume: 0,
				starch: 0,
				fat: 0,
				energy: 0,
				sugar: 0,
				sodium: 0,
				acidity: 0
			};
			return this.ingredients.reduce(function (sum, ingredient) {
				sum.weight += ingredient.weight;
				sum.volume += ingredient.weight / ingredient.density;
				sum.starch += ingredient.starch * ingredient.weight;
				sum.fat += ingredient.totalFat * ingredient.weight;
				sum.energy += ingredient.toalEnergy * ingredient.weight;
				sum.sugar += ingredient.sugar * ingredient.weight;
				sum.sodium += ingredient.sodium * ingredient.weight;
				sum.acidity += ingredient.acidityLevel * ingredient.weight;
				return sum;
			}, objDefault);
		}
	},
	methods: {
		onUpdateItem: function (newItem, item) {
			var index = this.ingredients.findIndex(function (ingredient) {
				return ingredient.id == newItem.id;
			});

			if (index === -1) return;
			return this.ingredients.splice(index, 1, newItem);
		}
	}
});